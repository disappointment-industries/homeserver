package fourohfour

import (
	"github.com/google/logger"
	"gitlab.com/MikeTTh/homeserver/utils"
	"html/template"
	"net/http"
)

var t403 *template.Template

func init() {
	utils.Try("403", func() error {
		var err error
		t403, err = utils.Parse(nil, "fourohfour/403.html")
		return err
	})
}

func Handle403(w http.ResponseWriter, r *http.Request) {
	wr := fourOhFourWriter{w, http.StatusForbidden}
	e := t403.Execute(wr, nil)
	if e != nil {
		logger.Errorf("403: %v", e)
	}
	wr.WriteHeader(wr.StatusHeader)
}
