package fourohfour

import (
	"encoding/json"
	"github.com/google/logger"
	"gitlab.com/MikeTTh/homeserver/utils"
	"html/template"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"sync"
	"time"
)

type fourOhFourWriter struct {
	http.ResponseWriter
	StatusHeader int
}

func (f fourOhFourWriter) WriteHeader(i int) {
	f.ResponseWriter.WriteHeader(f.StatusHeader)
}

var t404 *template.Template
var tSug *template.Template

func randQuote() template.HTML {
	quotes := []template.HTML{
		"Nem tudom hova tűnt 🤷‍♂️",
		"Lehet eltévedtél? 🤔",
		"But keep looking for it 😉",
		"<i>Where we're going, we don't need roads.</i>",
	}
	r := rand.Intn(len(quotes))
	return quotes[r]
}

func init() {
	utils.Try("404", func() error {
		var err error
		t404, err = utils.Parse(template.FuncMap{
			"randQuote": randQuote,
		}, "fourohfour/404.html")
		return err
	})
	utils.Try("404sug", func() error {
		var err error
		tSug, err = utils.Parse(nil, "fourohfour/suggest.html")
		return err
	})
}

type Suggestion struct {
	Path string
	Sug  string
	Date time.Time
}

var saveLck sync.Mutex

func saveSug(s *Suggestion) {
	saveLck.Lock()
	sugs := make([]Suggestion, 0)
	if by, er := ioutil.ReadFile("fourohfour/suggestions.json"); er == nil {
		if er := json.Unmarshal(by, &sugs); er != nil {
			logger.Errorf("%v", er)
		}
	} else {
		logger.Errorf("%v", er)
	}
	sugs = append(sugs, *s)
	if by, e := json.MarshalIndent(sugs, "", "    "); e == nil {
		if e := ioutil.WriteFile("fourohfour/suggestions.json", by, os.ModePerm); e != nil {
			logger.Errorf("%v", e)
		}
	}
	saveLck.Unlock()
}

func Handle404(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		if r.ParseForm() == nil {
			s := &Suggestion{
				Path: r.FormValue("path"),
				Sug:  r.FormValue("suggestion"),
				Date: time.Now(),
			}
			go saveSug(s)
			tSug.Execute(w, s)
		}
		return
	}

	wr := fourOhFourWriter{w, http.StatusNotFound}
	e := t404.Execute(wr, nil)
	if e != nil {
		logger.Errorf("404: %v", e)
	}
	wr.WriteHeader(wr.StatusHeader)
}
