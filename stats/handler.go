package stats

import (
	"encoding/json"
	"fmt"
	"github.com/google/logger"
	"gitlab.com/MikeTTh/homeserver/fourohfour"
	"gitlab.com/MikeTTh/homeserver/utils"
	"html/template"
	"math"
	"net/http"
	"reflect"
	"sort"
)

var mux = http.NewServeMux()
var tmpl *template.Template

type chartElem struct {
	Color    string
	Start    string
	Stop     string
	StartTxt string
	StopTxt  string
	Large    int
	Name     string
	Amount   uint64
	Percent  float64
}

type chart struct {
	Name  string
	Elems []chartElem
	Color string
}

var colors = []string{"#FF4050", "#F28144", "#FFD24A", "#A4CC35", "#26C99E", "#66BFFF", "#CC78FA", "#F553BF"}

type statHtml struct {
	Charts   []chart
	Requests uint64
	AvgTime  float64
}

func init() {
	mux.HandleFunc("/stats.json", handleJson)
	mux.HandleFunc("/", handle)
	utils.Try("stats", func() error {
		var err error
		tmpl, err = utils.Parse(nil, "stats/stats.html", "stats/chart.html")
		return err
	})
}

func Handler(w http.ResponseWriter, r *http.Request) {
	mux.ServeHTTP(w, r)
}

func handleJson(w http.ResponseWriter, r *http.Request) {
	by, e := json.Marshal(stat)
	if e != nil {
		logger.Errorf("stats: %v", e)
		return
	}
	_, _ = w.Write(by)
}

const diffTo100 = 0.00001

func mapToStruct(m map[string]uint64, max uint64) []chartElem {
	elems := make([]chartElem, 0, len(m))
	for k, v := range m {
		elems = append(elems, chartElem{
			Name:    k,
			Amount:  v,
			Percent: float64(v) / float64(max) * 100.0,
		})
	}
	sort.Slice(elems, func(i, j int) bool {
		return elems[i].Amount > elems[j].Amount
	})

	lastStop := "1 0"
	lastStopTxt := "0.8 0"
	sumPercent := 0.0
	for i := range elems {
		elems[i].Color = colors[i%len(colors)]
		elems[i].Start = lastStop
		elems[i].StartTxt = lastStopTxt
		sumPercent += elems[i].Percent
		if sumPercent >= 100.0-diffTo100 && i == 0 {
			sumPercent -= diffTo100
		}
		r := 2 * math.Pi * (sumPercent / 100.0)
		elems[i].Stop = fmt.Sprintf("%f %f", math.Cos(r), math.Sin(r))
		elems[i].StopTxt = fmt.Sprintf("%f %f", 0.8*math.Cos(r), 0.8*math.Sin(r))
		lastStop = elems[i].Stop
		lastStopTxt = elems[i].StopTxt
		if elems[i].Percent > 50 {
			elems[i].Large = 1
		}
	}
	return elems
}

func (s *Stats) htmlStruct() *statHtml {
	v := reflect.ValueOf(*s)
	t := v.Type()

	h := &statHtml{
		Charts: make([]chart, 0),
	}

	for i := 0; i < v.NumField(); i++ {
		if m, ok := v.Field(i).Interface().(map[string]uint64); ok {
			h.Charts = append(h.Charts, chart{
				Name:  t.Field(i).Name,
				Elems: mapToStruct(m, s.AllRequests),
				Color: colors[0],
			})
		}
	}

	for n, metric := range s.Metrics {
		m := metric()
		var all uint64 = 0
		for _, i := range m {
			all += i
		}
		h.Charts = append(h.Charts, chart{
			Name:  n,
			Elems: mapToStruct(m, all),
			Color: colors[0],
		})
	}

	h.Requests = s.AllRequests
	h.AvgTime = s.AllTime / float64(h.Requests)

	return h
}

func handle(w http.ResponseWriter, r *http.Request) {
	if r.URL.String() == "/" {
		str := stat.htmlStruct()
		e := tmpl.Execute(w, str)
		if e != nil {
			logger.Errorf("stats: %v", e)
		}
	} else {
		fourohfour.Handle404(w, r)
	}
}
