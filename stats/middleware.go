package stats

import (
	"encoding/json"
	"github.com/google/logger"
	"gitlab.com/MikeTTh/homeserver/login"
	"gitlab.com/MikeTTh/homeserver/utils"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

type RespWriter struct {
	http.ResponseWriter
	StatusHeader int
}

func (rw *RespWriter) WriteHeader(c int) {
	rw.StatusHeader = c
	rw.ResponseWriter.WriteHeader(c)
}

type Stats struct {
	Methods     map[string]uint64
	StatusCodes map[string]uint64
	Users       map[string]uint64
	AllTime     float64
	AllRequests uint64
	Module      map[string]uint64
	Metrics     map[string]Metric `json:"-"`
}

type Metric func() map[string]uint64

var stat = Stats{
	Methods:     make(map[string]uint64),
	StatusCodes: make(map[string]uint64),
	Users:       make(map[string]uint64),
	Module:      make(map[string]uint64),
	Metrics:     make(map[string]Metric),
	AllTime:     0,
	AllRequests: 0,
}

func AddMetric(name string, m Metric) {
	stat.Metrics[name] = m
}

func save(s Stats) {
	by, e := json.MarshalIndent(s, "", "    ")
	if e != nil {
		logger.Errorf("stats: %v", e)
	}
	_ = ioutil.WriteFile("stats/stats.json", by, os.ModePerm)
}

func init() {
	utils.Try("stats", func() error {
		by, e := ioutil.ReadFile("stats/stats.json")
		if e != nil {
			return e
		}
		e = json.Unmarshal(by, &stat)
		if e != nil {
			return e
		}
		return nil
	})
	go func() {
		for {
			time.Sleep(5 * time.Second)
			save(stat)
		}
	}()
	utils.AddToCleanup(func() {
		save(stat)
	})
}

var mutex sync.Mutex

func Middle(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		respw := &RespWriter{
			ResponseWriter: w,
		}

		h.ServeHTTP(respw, r)

		mutex.Lock()
		defer mutex.Unlock()

		if respw.StatusHeader == 0 {
			respw.StatusHeader = 200
		}

		url := r.URL.String()[1:]
		firstSlash := strings.Index(url, "/")
		if firstSlash != -1 {
			url = url[:firstSlash]
		}
		if len(url) == 0 {
			url = "/"
		}
		firstQuestion := strings.Index(url, "?")
		if firstQuestion != -1 {
			url = url[:firstQuestion]
		}

		stat.AllRequests++
		stat.Methods[r.Method]++
		stat.StatusCodes[strconv.Itoa(respw.StatusHeader)]++
		stat.Users[login.GetUser(r)]++
		stat.Module[url]++
		stat.AllTime += time.Since(start).Seconds()
	})
}
