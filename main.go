package main

import (
	"context"
	"crypto/tls"
	"flag"
	"fmt"
	"github.com/NYTimes/gziphandler"
	"github.com/google/logger"
	"github.com/tdewolff/minify"
	"github.com/tdewolff/minify/css"
	"github.com/tdewolff/minify/html"
	"github.com/tdewolff/minify/js"
	"github.com/tdewolff/minify/json"
	"github.com/tdewolff/minify/svg"
	"github.com/tdewolff/minify/xml"
	"gitlab.com/MikeTTh/homeserver/dash"
	"gitlab.com/MikeTTh/homeserver/fileserver"
	"gitlab.com/MikeTTh/homeserver/fourohfour"
	"gitlab.com/MikeTTh/homeserver/homepage"
	"gitlab.com/MikeTTh/homeserver/login"
	"gitlab.com/MikeTTh/homeserver/mateking"
	"gitlab.com/MikeTTh/homeserver/middle"
	"gitlab.com/MikeTTh/homeserver/stats"
	"gitlab.com/MikeTTh/homeserver/utils"
	"golang.org/x/crypto/acme/autocert"
	"net/http"
	"os"
	"regexp"
	"sync"
)

func favicon(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "res/favicon.ico")
}

func preinit() error {
	if _, err := os.Stat("homepage/index.html"); err != nil {
		if ex, err := os.Executable(); err == nil {
			for ex[len(ex)-1] != '/' {
				ex = ex[:len(ex)-1]
			}
			ex = ex[:len(ex)-1]

			return os.Chdir(ex)
		} else {
			return err
		}
	} else {
		return err
	}
}

var _ = preinit()

func hf(mux *http.ServeMux, prefix string, handler http.HandlerFunc) {
	mux.Handle(prefix+"/", http.StripPrefix(prefix, handler))
}

func main() {
	lf, err := os.OpenFile("files/log.txt", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0660)

	if os.IsNotExist(err) {
		_ = os.Mkdir("files", os.ModePerm)
		main()
		return
	}

	if err != nil {
		logger.Fatalf("Couldn't open log file: %v", err)
	}
	defer func() {
		_ = lf.Close()
	}()

	defer logger.Init("homeserver", true, true, lf).Close()

	debug := flag.Bool("debug", false, "Debug mode")
	flag.Parse()

	fs := fileserver.Fs(http.Dir("."))

	logHandler := func(w http.ResponseWriter, r *http.Request) {
		if login.CheckPriv(r, login.LogPriv) {
			fs.ServeHTTP(w, r)
		} else {
			fourohfour.Handle403(w, r)
		}
	}

	serveMux := http.NewServeMux()
	compressMux := http.NewServeMux()

	m := minify.New()
	m.AddFunc("text/css", css.Minify)
	m.AddFunc("text/html", html.Minify)
	m.AddFunc("image/svg+xml", svg.Minify)
	m.AddFuncRegexp(regexp.MustCompile("^(application|text)/(x-)?(java|ecma)script$"), js.Minify)
	m.AddFuncRegexp(regexp.MustCompile("[/+]json$"), json.Minify)
	m.AddFuncRegexp(regexp.MustCompile("[/+]xml$"), xml.Minify)

	compressMux.HandleFunc("/robots.txt", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "res/robots.txt")
	})
	compressMux.Handle("/res/", m.Middleware(fs))
	serveMux.HandleFunc("/files/log.txt", logHandler)
	serveMux.Handle("/files/", fs)
	compressMux.HandleFunc("/favicon.ico", favicon)

	hf(compressMux, "/login", login.Handler)
	hf(compressMux, "/dash", dash.Handler)
	hf(compressMux, "/stats", stats.Handler)
	hf(compressMux, "/mateking", mateking.Handler)
	hf(compressMux, "/mathxplain", mateking.Handler)
	compressMux.HandleFunc("/sites/", mateking.Handler)
	hf(compressMux, "/mateking/user", fourohfour.Handle404)
	compressMux.HandleFunc("/", homepage.ServeHome)
	serveMux.Handle("/", gziphandler.GzipHandler(compressMux))

	root := stats.Middle(serveMux)
	root = middle.Middle(root)

	httpServer := &http.Server{
		Handler: root,
		Addr:    ":8080",
	}
	if !*debug {
		httpServer.Addr = ":80"
		validUrl := []string{"mikewashere.sch.bme.hu", "tmiklos.sch.bme.hu", "mikesweb.site", "xn--bw8hnn.ws", "mike.sch.bme.hu"}
		hostPolicy := func(_ context.Context, host string) error {
			for _, u := range validUrl {
				cut := len(host) - len(u)
				if cut >= 0 {
					chost := host[cut:]
					if chost == u {
						return nil
					}
				}
			}
			return fmt.Errorf("acme/autocert: host %q not configured in HostWhitelist", host)

		}
		m := &autocert.Manager{
			Prompt:     autocert.AcceptTOS,
			HostPolicy: hostPolicy,
			Cache:      autocert.DirCache("keys"),
		}
		httpServer.Handler = m.HTTPHandler(nil)
		root = utils.HSTSHandler(root)
		httpsServer := &http.Server{
			Handler:   root,
			Addr:      ":443",
			TLSConfig: &tls.Config{GetCertificate: m.GetCertificate},
		}
		go func() {
			for true {
				err := httpsServer.ListenAndServeTLS("", "")
				logger.Errorf("%v", err)
			}
		}()
		torServer := &http.Server{
			Handler: root,
			Addr:    "localhost:8080",
		}
		go func() {
			for true {
				err := torServer.ListenAndServe()
				logger.Errorf("%v", err)
			}
		}()
	}
	go func() {
		for true {
			err := httpServer.ListenAndServe()
			logger.Errorf("%v", err)
		}
	}()
	var wg sync.WaitGroup
	wg.Add(1)
	wg.Wait()
}
