package homepage

import (
	"fmt"
	"github.com/google/logger"
	"gitlab.com/MikeTTh/homeserver/fourohfour"
	"gitlab.com/MikeTTh/homeserver/login"
	"gitlab.com/MikeTTh/homeserver/utils"
	"html/template"
	"io/ioutil"
	"net/http"
	"os/exec"
	"strings"
	"time"
)

var hp *template.Template

type home struct {
	Username string
	Onion    string
}

var onion string

func _init(first bool) {
	var err error
	hp, err = utils.Parse(nil, "homepage/index.html")
	if err != nil {
		if first {
			go func() {
				time.Sleep(time.Second)
				_init(false)
			}()
		} else {
			panic(err)
		}
	}

	if byt, e := ioutil.ReadFile("/var/lib/tor/hidden_service/hostname"); e == nil {
		onion = string(byt)
		onion = strings.Split(onion, ".onion")[0]
		onion += ".onion"
	}
}

func init() {
	_init(true)
}

func ServeHome(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
	case "/", "/index", "/index.html", "/index.php":
		h := &home{
			Username: login.GetUser(r),
			Onion:    onion,
		}
		e := hp.Execute(w, h)
		if e != nil {
			logger.Errorf("homepage: %v", e)
		}
		return
	case "/shell":
		shell(w, r)
		return
	default:
		fourohfour.Handle404(w, r)
		return
	}
}

func shell(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		fourohfour.Handle404(w, r)
		return
	}
	by, e := ioutil.ReadAll(r.Body)
	if e == nil {
		_ = r.Body.Close()
		cmd := string(by)
		prog := cmd
		args := ""
		if strings.Contains(cmd, " ") {
			prog = strings.Split(prog, " ")[0]
			args = cmd[len(prog)+1:]
		}
		if prog == "help" {
			if strings.TrimSpace(args) == "" {
				for _, c := range commands {
					if c.Help != "" {
						_, _ = w.Write([]byte(fmt.Sprintf("%s: %s\n", c.Name, c.Help)))
					}
				}
			} else {
				found := false
				for _, c := range commands {
					if c.Name == args && c.Help != "" {
						_, _ = w.Write([]byte(fmt.Sprintf("%s: %s", c.Name, c.Help)))
						found = true
						break
					}
				}
				if !found {
					_, _ = w.Write([]byte(fmt.Sprintf(`help: error: command "%s" not found or invalid syntax
Usage: help [command]`, args)))
				}
			}
		} else {
			found := false
			for _, c := range commands {
				if c.Name == prog {
					found = true
					c.Do(w, r, args)
					break
				}
			}
			if !found {
				_, _ = w.Write([]byte(fmt.Sprintf("command %s not found, use help for a list of commands", prog)))
			}
		}
	} else {
		_, _ = w.Write([]byte(e.Error()))
	}
}

type command struct {
	Name string
	Help string
	Do   func(w http.ResponseWriter, r *http.Request, args string)
}

var commands = []command{
	{
		Name: "ls",
		Help: "list available pages",
		Do: func(w http.ResponseWriter, r *http.Request, args string) {
			_, _ = w.Write([]byte(`<a href="/">.</a>
<a href="/files">Files</a>
<a href="/dash">Dashboard</a>
<a href="/stats">Statistics</a>`))
			if login.CheckPriv(r, login.MatekingPriv) {
				_, _ = w.Write([]byte(`
<a href="/mateking">Mateking</a>`))
			}
			_, _ = w.Write([]byte(`
welcome.txt`))
			if onion != "" {
				_, _ = w.Write([]byte(`
onion.txt`))
			}
		},
	},
	{
		Name: "uname",
		Help: "os info",
		Do: func(w http.ResponseWriter, r *http.Request, args string) {
			arg := []string{"-snrmo"}
			if len(args) > 0 {
				arg = strings.Split(args, " ")
			}
			if by, e := exec.Command("uname", arg...).CombinedOutput(); e == nil {
				_, _ = w.Write(by)
			} else {
				_, _ = w.Write(by)
			}
		},
	},
	{
		Name: "sudo",
		Do: func(w http.ResponseWriter, r *http.Request, args string) {
			if args == "" {
				_, _ = w.Write([]byte(`usage: sudo <command>`))
			} else {
				user := login.GetUser(r)
				if login.CheckPriv(r, login.SudoPriv) {
					arg := strings.Split(args, " ")
					if by, e := exec.Command(arg[0], arg[1:]...).CombinedOutput(); e == nil {
						_, _ = w.Write(by)
					} else {
						_, _ = w.Write(by)
					}
				} else {
					_, _ = w.Write([]byte(fmt.Sprintf(`user %s not in the sudoers file.  This incident will be reported.`, user)))
				}
			}
		},
	},
	{
		Name: "cat",
		Help: "type out text file",
		Do: func(w http.ResponseWriter, r *http.Request, args string) {
			if args == "" {
				_, _ = w.Write([]byte(`usage: cat <file>`))
			} else {
				if args == "welcome.txt" {
					ip := r.RemoteAddr[:strings.LastIndexAny(r.RemoteAddr, ":")]
					if ip == "127.0.0.1" || ip == "[::1]]" {
						ip = "Tor user"
					}
					_, _ = w.Write([]byte(fmt.Sprintf("Hello %s, welcome to my website!", ip)))
				} else if onion != "" && args == "onion.txt" {
					_, _ = w.Write([]byte(fmt.Sprintf(`This website is also available at
<a href="http://%s">%s</a>
via the Tor network`, onion, onion)))
				} else {
					_, _ = w.Write([]byte(fmt.Sprintf("cat: file %s not found", args)))
				}
			}
		},
	},
	{
		Name: "login",
		Help: "log in",
		Do: func(w http.ResponseWriter, r *http.Request, args string) {
			next := login.Encode("/")
			_, _ = w.Write([]byte(`<form action="/login/?next=` + next + `" method="post">
	<input type="text" name="username">
	<input type="password" name="password">
	<input type="submit" value="submit">
</form>`))
		},
	},
	{
		Name: "exit",
		Do: func(w http.ResponseWriter, r *http.Request, args string) {
			_, _ = w.Write([]byte(`You cannot escape, now you must suffer!
<iframe width="100%" height="100%" src="/res/rick.mp4" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`))
		},
	},
	{
		Name: "thecake",
		Help: "is a lie",
		Do: func(w http.ResponseWriter, r *http.Request, args string) {
			_, _ = w.Write([]byte(`              .,-:;//;:=,
          . :H@@@MM@M#H/.,+%;,
       ,/X+ +M@@M@MM%=,-%HMMM@X/,
     -+@MM; $M@@MH+-,;XMMMM@MMMM@+-
    ;@M@@M- XM@X;. -+XXXXXHHH@M@M#@/.
  ,%MM@@MH ,@%=            .---=-=:=,.
  =@#@@@MX .,              -%HX$$%%%+;
 =-./@M@M$                  .;@MMMM@MM:
 X@/ -$MM/                    .+MM@@@M$
,@M@H: :@:                    . =X#@@@@-
,@@@MMX, .                    /H- ;@M@M=
.H@@@@M@+,                    %MM+..%#$.
 /MMMM@MMH/.                  XM@MH; =;
  /%+%$XHH@$=              , .H@@@@MX,
   .=--------.           -%H.,@@@@@MX,
   .%MM@@@HHHXX$$$%+- .:$MMX =M@@MM%.
     =XMMM@MM@MM#H;,-+HMM@M+ /MMMX=
       =%@M@M#@$-.=$@MM@@@M; %M%=
         ,:+$+-,/H#MMMMMMM@= =,
               =++%%%%+/:-.             
<b>Aperture Labs</b>
<i>We do what we must, because we can</i>`))
		},
	},
}
