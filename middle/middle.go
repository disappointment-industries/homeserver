package middle

import (
	"bytes"
	"github.com/google/logger"
	"gitlab.com/MikeTTh/homeserver/login"
	"io/ioutil"
	"net/http"
	"time"
)

type RespWriter struct {
	http.ResponseWriter
	StatusHeader int
}

func (rw *RespWriter) WriteHeader(c int) {
	rw.StatusHeader = c
	rw.ResponseWriter.WriteHeader(c)
}

func Middle(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		extra := ""
		if r.Method == http.MethodPost {
			ex, e := ioutil.ReadAll(r.Body)
			if e == nil {
				extra = "{"
				extra += string(ex)
				extra += "} "
				_ = r.Body.Close()
				r.Body = ioutil.NopCloser(bytes.NewBuffer(ex))
			}
		}

		respw := &RespWriter{
			ResponseWriter: w,
		}
		h.ServeHTTP(respw, r)
		if respw.StatusHeader == 0 {
			respw.StatusHeader = 200
		}

		logger.Infof("%s (%s) -> %s %s%s %s(%d, %fs)", r.RemoteAddr, login.GetUser(r), r.Method, r.Host, r.URL.String(), extra, respw.StatusHeader, time.Since(start).Seconds())
	})
}
