package login

import (
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"github.com/google/logger"
	"gitlab.com/MikeTTh/homeserver/fourohfour"
	"gitlab.com/MikeTTh/homeserver/utils"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
)

const (
	Cookiename = "uid"
)

type Privilege int

const (
	MatekingPriv Privilege = iota
	LogPriv
	SudoPriv
	MatekingManagerPriv
)

type User struct {
	Hash       string
	Salt       string
	Privileges []Privilege
}

func CheckPriv(r *http.Request, priv Privilege) bool {
	if u, ok := ValidUsers[GetUser(r)]; ok {
		if u.HasPrivilege(priv) {
			return true
		}
	}
	return false
}

func (u *User) HasPrivilege(priv Privilege) bool {
	has := false
	for _, p := range u.Privileges {
		if priv == p {
			has = true
		}
	}
	return has
}

func GetUser(r *http.Request) string {
	n := "guest"
	if c, e := r.Cookie(Cookiename); e == nil {
		if name, ok := ValidCookies[c.Value]; ok {
			n = name
		}
	}
	return n
}

var s = sha256.New()

func hash(in, salt string) string {
	return hex.EncodeToString(s.Sum([]byte(in + salt)))
}

func Encode(in string) string {
	return base64.StdEncoding.EncodeToString([]byte(in))
}

func Decode(in string) string {
	by, e := base64.StdEncoding.DecodeString(in)
	if e != nil {
		logger.Error(e)
		return ""
	}
	return string(by)
}

var ValidUsers = map[string]User{
	"mike": {
		Salt:       "kfljdsfkj",
		Privileges: []Privilege{SudoPriv, MatekingPriv, LogPriv, MatekingManagerPriv},
	},
	"friend": {
		Salt:       "hello",
		Privileges: []Privilege{MatekingPriv},
	},
	"guest": {},
}

func init() {
	utils.Try("login", func() error {
		if byt, e := ioutil.ReadFile("login/usernames"); e == nil {
			entries := strings.Split(string(byt), "\n")
			for _, e := range entries {
				ind := strings.Index(e, "=")
				if ind == -1 {
					continue
				}
				if u, ok := ValidUsers[e[:ind]]; ok {
					u.Hash = hash(e[ind+1:], u.Salt)
					ValidUsers[e[:ind]] = u
				}
			}
		} else {
			return e
		}
		return nil
	})
	utils.Try("login", func() error {
		b, e := ioutil.ReadFile("login/cookies.json")
		if e != nil {
			return e
		}
		e = json.Unmarshal(b, &ValidCookies)
		if e != nil {
			return e
		}
		return nil
	})
}

var ValidCookies = make(map[string]string)

var mutex sync.Mutex

func saveCookies() error {
	mutex.Lock()
	defer mutex.Unlock()
	out, err := json.MarshalIndent(ValidCookies, "", "    ")
	if err != nil {
		return err
	}
	err = ioutil.WriteFile("login/cookies.json", out, os.ModePerm)
	if err != nil {
		return err
	}
	return nil
}

func Handler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		fourohfour.Handle403(w, r)
		return
	}

	e := r.ParseForm()
	if e != nil {
		_, _ = w.Write([]byte(e.Error()))
		return
	}

	uname := r.FormValue("username")
	if u, ok := ValidUsers[uname]; ok {
		if u.Hash == hash(r.FormValue("password"), u.Salt) {
			ra := rand.Int()
			ras := strconv.Itoa(ra)
			c := &http.Cookie{
				Path:  "/",
				Name:  Cookiename,
				Value: ras,
			}
			http.SetCookie(w, c)
			c.Path = "/mateking"
			http.SetCookie(w, c)
			c.Path = "/mathxplain"
			http.SetCookie(w, c)
			if r.URL.Query().Get("next") != "" {
				http.Redirect(w, r, Decode(r.URL.Query().Get("next")), http.StatusFound)
			} else {
				_, _ = w.Write([]byte("ok"))
			}
			ValidCookies[ras] = uname
			go func() {
				err := saveCookies()
				if err != nil {
					logger.Errorf("%v", err)
				}
			}()
			return
		}
	}

	_, _ = w.Write([]byte("error"))
}
