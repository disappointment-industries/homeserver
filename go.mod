module gitlab.com/MikeTTh/homeserver

go 1.13

replace gitlab.com/MikeTTh/homeserver => ./

require (
	github.com/NYTimes/gziphandler v1.1.1
	github.com/google/logger v1.1.0
	github.com/mackerelio/go-osstat v0.1.0
	github.com/tdewolff/minify v2.3.6+incompatible
	github.com/tdewolff/parse v2.3.4+incompatible // indirect
	github.com/tdewolff/test v1.0.6 // indirect
	golang.org/x/crypto v0.0.0-20200728195943-123391ffb6de
	golang.org/x/net v0.0.0-20200707034311-ab3426394381
	golang.org/x/sys v0.0.0-20200812155832-6a926be9bd1d // indirect
	golang.org/x/text v0.3.3 // indirect
)
