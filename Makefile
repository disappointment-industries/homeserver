GOROOT = $(shell go env GOROOT)

all: go_dash main

main:
	go build

wasm_exec:
	cp $(GOROOT)/misc/wasm/wasm_exec.js res/

go_dash: wasm_exec
	bash -c "cd dash/clientside; GOOS=js GOARCH=wasm go build -o ../app.wasm"

git:
	git pull

systemd:
	sudo systemctl restart homeserver

update: git all systemd

clean:
	rm -f homeserver dash/app.wasm games/dream/app.wasm res/wasm_exec.js