package utils

import (
	"github.com/google/logger"
	"time"
)

type TryFn func() error

func Try(name string, f TryFn) {
	if f() != nil {
		go func() {
			time.Sleep(time.Second)
			e := f()
			if e != nil {
				logger.Errorf(name+": %v", e)
			}
		}()
	}
}
