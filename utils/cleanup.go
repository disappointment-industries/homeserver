package utils

import (
	"os"
	"os/signal"
	"syscall"
)

type CleanupFunc func()

var clup = make([]CleanupFunc, 0)

func AddToCleanup(f CleanupFunc) {
	clup = append(clup, f)
}

func reverse(fs []CleanupFunc) []CleanupFunc {
	j := 0
	ret := make([]CleanupFunc, len(fs), len(fs))
	for i := len(fs) - 1; i >= 0; i-- {
		ret[j] = fs[i]
		j++
	}
	return ret
}

func init() {
	c := make(chan os.Signal)
	signal.Notify(c, syscall.SIGTERM)
	signal.Notify(c, os.Interrupt)
	go func() {
		for _ = range c {
			for _, f := range reverse(clup) {
				f()
			}
			os.Exit(0)
		}
	}()
}
