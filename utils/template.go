package utils

import (
	"errors"
	"flag"
	"github.com/tdewolff/minify"
	"github.com/tdewolff/minify/css"
	"github.com/tdewolff/minify/html"
	"github.com/tdewolff/minify/js"
	"github.com/tdewolff/minify/json"
	"github.com/tdewolff/minify/xml"
	"html/template"
	"io/ioutil"
	"path/filepath"
	"regexp"
	"strings"
)

var flagError = errors.New("flags not initialized yet")

func Parse(funcMap template.FuncMap, filenames ...string) (*template.Template, error) {
	lu := flag.Lookup("debug")
	if lu == nil {
		return nil, flagError
	}
	if g, ok := lu.Value.(flag.Getter); !ok {
		return nil, flagError
	} else {
		if d, ok := g.Get().(bool); !ok {
			return nil, flagError
		} else {
			if d {
				name := filepath.Base(filenames[0])
				return template.New(name).Funcs(funcMap).ParseFiles(filenames...)
			} else {
				m := minify.New()
				m.AddFunc("text/css", css.Minify)
				m.AddFunc("text/html", html.Minify)
				//m.AddFunc("image/svg+xml", svg.Minify)
				m.AddFuncRegexp(regexp.MustCompile("^(application|text)/(x-)?(java|ecma)script$"), js.Minify)
				m.AddFuncRegexp(regexp.MustCompile("[/+]json$"), json.Minify)
				m.AddFuncRegexp(regexp.MustCompile("[/+]xml$"), xml.Minify)

				var tmpl *template.Template
				for _, filename := range filenames {
					name := filepath.Base(filename)
					var t *template.Template
					if tmpl == nil {
						tmpl = template.New(name).Funcs(funcMap)
						t = tmpl
					} else {
						t = tmpl.New(name)
					}

					b, err := ioutil.ReadFile(filename)
					if err != nil {
						return nil, err
					}

					mb, err := m.Bytes("text/html", b)
					if err != nil {
						return nil, err
					}

					// Fix some errors that minify causes for templating
					s := string(mb)
					s = strings.ReplaceAll(s, "{{{", "{ {{")
					s = strings.ReplaceAll(s, "}}}", "}} }")

					_, _ = t.Parse(s)
				}
				return tmpl, nil
			}
		}
	}
}
