package mateking

import (
	"encoding/json"
	"github.com/google/logger"
	"gitlab.com/MikeTTh/homeserver/fourohfour"
	"gitlab.com/MikeTTh/homeserver/login"
	"gitlab.com/MikeTTh/homeserver/utils"
	"html/template"
	"io/ioutil"
	"net/http"
	"time"
)

var tMgr *template.Template

func init() {
	utils.Try("matekingMgr", func() error {
		var err error
		tMgr, err = utils.Parse(nil, "mateking/mgr.html")
		return err
	})
}

type manager struct {
	JSON string
}

func handleManager(w http.ResponseWriter, r *http.Request) {
	if !(r.URL.Path == "/" || r.URL.Path == "") {
		fourohfour.Handle404(w, r)
		return
	}

	if !login.CheckPriv(r, login.MatekingManagerPriv) {
		fourohfour.Handle403(w, r)
		return
	}

	if r.Method == http.MethodPost {
		b, e := ioutil.ReadAll(r.Body)
		if e != nil {
			logger.Error(e)
			return
		}

		var m Mateking
		e = json.Unmarshal(b, &m)
		if e != nil {
			logger.Error(e)
			return
		}

		matek.Users = m.Users
		users = m.Users

		_, _ = w.Write([]byte("OK"))

		go func() {
			for getCookies() != nil {
				time.Sleep(time.Second)
			}
		}()

		return
	}

	b, e := json.Marshal(matek)
	if e != nil {
		logger.Error(e)
		return
	}

	man := &manager{
		JSON: string(b),
	}

	e = tMgr.Execute(w, man)
	if e != nil {
		logger.Errorf("mgr: %v", e)
	}
}
