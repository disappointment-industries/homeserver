package mateking

import (
	"github.com/google/logger"
	"gitlab.com/MikeTTh/homeserver/utils"
	"html/template"
	"net/http"
)

var tbanned *template.Template

func init() {
	utils.Try("403", func() error {
		var err error
		tbanned, err = utils.Parse(nil, "mateking/banned.html")
		return err
	})
}

func handleBanned(w http.ResponseWriter, r *http.Request) {
	e := tbanned.Execute(w, nil)
	logger.Error(e)
}
