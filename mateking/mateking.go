package mateking

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/google/logger"
	"gitlab.com/MikeTTh/homeserver/fourohfour"
	"gitlab.com/MikeTTh/homeserver/login"
	"gitlab.com/MikeTTh/homeserver/stats"
	"gitlab.com/MikeTTh/homeserver/utils"
	"golang.org/x/net/publicsuffix"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"os"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"
)

var hopHeaders = []string{
	"Accept-Encoding",
	"Connection",
	"Keep-Alive",
	"Proxy-Authenticate",
	"Proxy-Authorization",
	"Te", // canonicalized version of "TE"
	"Trailers",
	"Transfer-Encoding",
	"Upgrade",
	"Content-Length",
}

func copyHeader(dst, src http.Header) {
	for k, vv := range src {
		for _, v := range vv {
			dst.Add(k, v)
		}
	}
}

func delHopHeaders(header http.Header) {
	for _, h := range hopHeaders {
		header.Del(h)
	}
}

var client = &http.Client{
	CheckRedirect: func(req *http.Request, via []*http.Request) error {
		return nil
	},
}

func parentDir(path string) string {
	index := strings.LastIndex(path, "/")
	return path[:index]
}

func createParentDirs(path string) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		err := os.MkdirAll(parentDir(path), os.ModePerm)
		if err != nil {
			logger.Errorf("mateking: %v", err)
		}
	}
}

var mut sync.Mutex

type cache struct {
	sync.Mutex `json:"-"`
	lastSave   time.Time
	Items      map[string]*cacheItem
}

func (c *cache) Save() {
	if c.Items == nil {
		return
	}

	if time.Since(c.lastSave) > 0 {
		c.lastSave = time.Now().Add(10 * time.Second)
		go func() {
			time.Sleep(10 * time.Second)
			c.Lock()
			out, err := json.MarshalIndent(c, "", "    ")
			if err != nil {
				logger.Errorf("mateking: %v", err)
			}
			err = ioutil.WriteFile("mateking/cache/cache.json", out, os.ModePerm)
			if err != nil {
				logger.Errorf("mateking: %v", err)
			}
			c.Unlock()
		}()
	}
}

func (c *cache) Load() error {
	b, e := ioutil.ReadFile("mateking/cache/cache.json")
	if e != nil {
		return e
	}
	e = json.Unmarshal(b, c)
	if e != nil {
		return e
	}
	return nil
}

func (c *cache) Push(rem, loc string) {
	c.Lock()
	if c.Items == nil {
		c.Items = make(map[string]*cacheItem)
	}
	c.Items[rem] = &cacheItem{
		Remote:  rem,
		Local:   loc,
		Updated: time.Now(),
	}
	c.Unlock()
	c.Save()
}

func (c *cache) FindRemoteURL(local string) string {
	for _, i := range c.Items {
		if i.Local == local {
			return i.Remote
		}
	}
	return ""
}

func (c *cache) At(remote string) *cacheItem {
	if i, ok := c.Items[remote]; ok {
		return i
	}
	return nil
}

type cacheItem struct {
	Remote  string
	Local   string
	Updated time.Time
}

var cac cache
var check = "másik eszközön vagy másik böngészőben ugyanezzel a névvel"

func randSave(orig, p string, data []byte) {
	p = p[:strings.LastIndex(p, "/")]
	p += strconv.Itoa(rand.Int())
	err := ioutil.WriteFile(p, data, os.ModePerm)
	if err == nil {
		cac.Push(orig, p)
	} else {
		logger.Errorf("mateking: %v", err)
	}
}

func saveMatek(orig, path string, data []byte) {
	if strings.Contains(string(data), check) {
		go func() {
			for getCookies() != nil {
				time.Sleep(time.Second)
			}
		}()
		return
	}

	p := "mateking/cache" + path
	if path == "/" {
		p = "mateking/cache/index.html"
	}
	createParentDirs(p)
	mut.Lock()
	err := ioutil.WriteFile(p, data, os.ModePerm)
	if err == nil {
		cac.Push(orig, p)
	}
	mut.Unlock()
	if e, ok := err.(*os.PathError); ok && e.Err == syscall.EISDIR {
		saveMatek(orig, path+"/index.html", data)
	} else if e, ok := err.(*os.PathError); ok && e.Err == syscall.ENOTDIR {
		lastSlash := strings.LastIndex(p, "/")
		par := p[:lastSlash]
		mut.Lock()
		err = os.Rename(par, par+".tmp")
		if err != nil {
			logger.Errorf("mateking: renaming failed %v", err)
			randSave(orig, p, data)
		}
		err = os.Mkdir(par, os.ModePerm)
		if err != nil {
			logger.Errorf("mateking: mkdir failed %v", err)
			randSave(orig, p, data)
		}
		err = os.Rename(par+".tmp", par+"/index.html")
		if err != nil {
			logger.Errorf("mateking: renaming failed, %v", err)
			randSave(orig, p, data)
		}
		cac.Push(cac.FindRemoteURL(par), par+"/index.html")
		mut.Unlock()
		saveMatek(orig, path, data)
	} else if err != nil {
		randSave(orig, p, data)
	}
}

var lastChk = make(map[string]time.Time)

func checkCache(w http.ResponseWriter, r *http.Request) error {
	mut.Lock()
	defer mut.Unlock()
	if i := cac.At(r.URL.String()); i != nil {
		if t, ok := lastChk[r.URL.String()]; ok {
			if time.Since(t) < 3*time.Second {
				logger.Infof("Invalidating cache %s", r.URL.String())
				return fmt.Errorf("fast checks, trigger reload now")
			}
		}

		http.ServeFile(w, r, i.Local)
		if f, err := os.Open(i.Local); err == nil {
			buf := make([]byte, 512)
			if _, err := f.Read(buf); err == nil {
				if http.DetectContentType(buf) == "text/html; charset=utf-8" {
					_, _ = w.Write([]byte(fmt.Sprintf(`<script>document.title += " | Mentve: %d/%d/%d"</script>`, i.Updated.Year(), int(i.Updated.Month()), i.Updated.Day())))
				}
			}
		}

		matek.CacheHit++
		lastChk[r.URL.String()] = time.Now()
		return nil
	}

	return fmt.Errorf("not cached")
}

func findUserForURL(url string) *User {
	for i, u := range users {
		for _, scope := range u.Scope {
			if strings.Contains(url, scope) {
				client.Jar = u.Jar
				return &users[i]
			}
		}
	}
	return nil
}

func Handler(w http.ResponseWriter, r *http.Request) {
	if !login.CheckPriv(r, login.MatekingPriv) {
		fourohfour.Handle403(w, r)
		return
	}

	if r.URL.Path == "/manager" {
		http.StripPrefix("/manager", http.HandlerFunc(handleManager)).ServeHTTP(w, r)
		return
	}

	matek.Requests++

	if e := checkCache(w, r); e == nil {
		return
	}

	uname := ""

	u := findUserForURL(r.URL.String())

	if u != nil {
		client.Jar = u.Jar
		uname = u.Email
	} else if ref := r.Header.Get("Referer"); ref != "" {
		u = findUserForURL(ref)
		if u != nil {
			client.Jar = u.Jar
			uname = u.Email
		}
	}

	if uname == "-" {
		handleBanned(w, r)
		return
	}

	dom := r.Host
	proto := "https"
	if r.TLS == nil {
		proto = "http"
	}
	var body url.Values
	var req *http.Request
	var err error

	if r.Method == "POST" {
		_ = r.ParseForm()
		body = r.PostForm
		req, err = http.NewRequest(r.Method, "https://mateking.hu"+r.URL.String(), strings.NewReader(body.Encode()))
	} else {
		req, err = http.NewRequest(r.Method, "https://mateking.hu"+r.URL.String(), nil)
	}
	if err != nil {
		panic(err)
	}

	delHopHeaders(r.Header)
	copyHeader(req.Header, r.Header)

	if ref := r.Header.Get("Referer"); ref != "" {
		ref = strings.ReplaceAll(ref, proto+"://"+dom+"/mateking", "https://www.mateking.hu")
		ref = strings.ReplaceAll(ref, proto+"://"+dom+"/mathxplain", "https://cdn.mathxplain.com")
		r.Header.Set("Referer", ref)
	}

	resp, err := client.Do(req)
	if err != nil {
		logger.Error(err)
	}
	defer func() {
		_ = resp.Body.Close()
	}()

	by, _ := ioutil.ReadAll(resp.Body)
	str := string(by)
	str = strings.ReplaceAll(str, "https://www.mateking.hu", proto+"://"+dom+"/mateking")
	str = strings.ReplaceAll(str, "https:\\/\\/www.mateking.hu", proto+":\\/\\/"+dom+"\\/mateking")

	str = strings.ReplaceAll(str, "https://cdn.mathxplain.com", proto+"://"+dom+"/mathxplain")
	str = strings.ReplaceAll(str, "https:\\/\\/cdn.mathxplain.com", proto+":\\/\\/"+dom+"\\/mathxplain")

	// only for DRM
	str = strings.ReplaceAll(str, "http://dev.mateking.hu", proto+"://"+dom)

	copyHeader(w.Header(), resp.Header)

	if resp.StatusCode >= 200 && resp.StatusCode < 400 {
		go saveMatek(r.URL.String(), r.URL.String(), []byte(str))
	}

	ind := strings.Index(str, check)
	if ind > 0 {
		start := ind
		for str[start] != '>' {
			start--
		}
		start++
		stop := ind
		for str[stop] != '<' {
			stop++
		}
		sub := str[start:stop]

		str = strings.ReplaceAll(str, sub, "✨ Sajnos ahhoz, hogy túljárjak a mateking agyán frissítened kell az oldalt vagy várnod kell öt percet. ✨")
	}

	if http.DetectContentType([]byte(str)) == "text/html; charset=utf-8" {
		str += fmt.Sprintf(`<script>document.title += " | Újratöltve (%s)"</script>`, uname)
	}

	_, _ = w.Write([]byte(str))
	w.WriteHeader(resp.StatusCode)
}

type Mateking struct {
	Users    []User
	CacheHit uint64
	Requests uint64
}

func (m *Mateking) Load() error {
	b, e := ioutil.ReadFile("mateking/cache/mateking.json")
	if e != nil {
		return e
	}
	e = json.Unmarshal(b, m)
	if e != nil {
		return e
	}
	return nil
}

func (m *Mateking) Save() {
	out, err := json.MarshalIndent(m, "", "    ")
	if err != nil {
		logger.Errorf("mateking: %v", err)
	}
	err = ioutil.WriteFile("mateking/cache/mateking.json", out, os.ModePerm)
	if err != nil {
		logger.Errorf("mateking: %v", err)
	}
}

var users []User

func getCookies() error {
	errorString := ""
	for i := range users {
		er := users[i].Login()
		if er != nil {
			errorString += er.Error()
			logger.Infof("%s failed to log in", users[i].Email)
		}
		// TODO: Add checks to see if auth was successful
	}
	if errorString == "" {
		return nil
	}
	return errors.New(errorString)
}

type User struct {
	Email string
	Pw    string
	Scope []string
	Jar   *cookiejar.Jar `json:"-"`
}

func (u *User) Login() error {
	if u.Email == "-" {
		return nil
	}
	options := cookiejar.Options{
		PublicSuffixList: publicsuffix.List,
	}
	if jar, err := cookiejar.New(&options); err == nil {
		u.Jar = jar
		form := url.Values{}
		form.Add("name", u.Email)
		form.Add("pass", u.Pw)
		form.Add("form_id", "user_login_block")
		form.Add("op", "Bejelentkezés")
		client2 := &http.Client{
			Jar: jar,
			CheckRedirect: func(req *http.Request, via []*http.Request) error {
				return http.ErrUseLastResponse
			},
		}
		if resp, err := client2.PostForm("https://www.mateking.hu/node/10?destination=node/10", form); err == nil {
			_ = resp.Body.Close()
			return nil
		} else {
			return err
		}
	} else {
		return err
	}
}

var matek Mateking

func init() {
	utils.Try("mateking", matek.Load)
	users = matek.Users
	go func() {
		for {
			time.Sleep(time.Minute)
			matek.Save()
		}
	}()
	utils.AddToCleanup(matek.Save)
	utils.AddToCleanup(cac.Save)

	utils.Try("mateking", cac.Load)
	go func() {
		for {
			for getCookies() != nil {
				time.Sleep(time.Second)
			}
			time.Sleep(5 * time.Minute)
		}
	}()

	stats.AddMetric("Mateking cache", func() map[string]uint64 {
		m := make(map[string]uint64)
		m["Hit"] = matek.CacheHit
		m["Miss"] = matek.Requests - matek.CacheHit
		return m
	})
}
