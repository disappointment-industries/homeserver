// +build !js

package dash

import (
	"encoding/json"
	"fmt"
	"github.com/mackerelio/go-osstat/cpu"
	"github.com/mackerelio/go-osstat/memory"
	"gitlab.com/MikeTTh/homeserver/fourohfour"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

var prevCpu *cpu.Stats

const (
	B  = 1
	KB = 1024 * B
	MB = 1024 * KB
	GB = 1024 * MB
)

func Handler(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
	case "/":
		if pusher, ok := w.(http.Pusher); ok {
			if err := pusher.Push("/res/wasm_exec.js", nil); err != nil {
				log.Printf("Push failed %v\n", err)
			}
			if err := pusher.Push("/dash/app.wasm", nil); err != nil {
				log.Printf("Push failed %v\n", err)
			}
		}
		http.ServeFile(w, r, "dash/dash.html")
		break
	case "/app.wasm":
		http.ServeFile(w, r, "dash/app.wasm")
		break
	case "/app.js":
		http.ServeFile(w, r, "dash/app.js")
		break
	case "/status":
		_, _ = w.Write(statusResponse())
		w.WriteHeader(200)
		break
	default:
		fourohfour.Handle404(w, r)
		return
	}
}

func getDiskUsage() []Disk {
	df := exec.Command("df", "-B", "1")
	if out, err := df.Output(); err == nil {
		str := string(out)
		for strings.Contains(str, "  ") {
			str = strings.ReplaceAll(str, "  ", " ")
		}
		rows := strings.Split(str, "\n")
		rows = rows[1 : len(rows)-1]
		disks := make([]Disk, 0, len(rows))
		for _, l := range rows {
			words := strings.Split(l, " ")
			var temp Disk
			temp.Name = words[5]
			if u, err := strconv.Atoi(words[2]); err == nil {
				temp.Used = uint64(u)
			} else {
				continue
			}
			if a, err := strconv.Atoi(words[3]); err == nil {
				temp.Avail = uint64(a)
			} else {
				continue
			}

			if temp.Used > GB {
				disks = append(disks, temp)
			}
		}
		return disks
	}
	return nil
}

func getDataUsed() uint {
	if data, err := http.Get("https://netwatcher.sch.bme.hu"); err == nil {
		if bod, err := ioutil.ReadAll(data.Body); err == nil {
			str := string(bod)
			if !strings.Contains(str, "32 GB") {
				return 0
			}
			split := strings.Split(str, " / 32 GB")
			debloat := split[0]
			debloat = strings.ReplaceAll(debloat, " ", "")
			debloat = strings.ReplaceAll(debloat, "\n", "")
			debloat = strings.ReplaceAll(debloat, "\r", "")
			split2 := strings.Split(debloat, "<!--4daywindow-->")
			scope := split2[1]
			unit := scope[len(scope)-2:]
			num := scope[:len(scope)-2]
			ret := 0.0
			if _, err = fmt.Sscanf(num, "%f", &ret); err == nil {
				switch unit {
				case "GB":
					ret *= GB
					break
				case "MB":
					ret *= MB
					break
				case "KB":
					ret *= KB
					break
				default:
					break
				}
				return uint(ret)
			} else {
				fmt.Println(err)
			}
		}
	}
	return 0
}

func statusResponse() []byte {
	str := &StatusData{
		Time: time.Now(),
	}

	cp, mu, mt := 0.0, uint64(0), uint64(0)

	if c, e := cpu.Get(); e == nil {
		if prevCpu != nil {
			cp = 1.0 - float64(c.Idle-prevCpu.Idle)/float64(c.Total-prevCpu.Total)
		}
		prevCpu = c
	}

	if m, e := memory.Get(); e == nil {
		mu, mt = m.Used, m.Total
	}

	name := "Server"

	if n, err := os.Hostname(); err == nil {
		name = n
	}

	tx := getDataUsed()

	disks := getDiskUsage()

	str.Nodes = append(str.Nodes, Node{
		Name:     name,
		CPU:      cp,
		MemUsed:  mu,
		MemTotal: mt,
		Sent:     uint64(tx),
		Storage:  disks,
	})

	by, er := json.Marshal(str)
	if er != nil {
		return []byte("")
	}
	return by
}
