package main

import (
	"github.com/gopherjs/vecty"
	"github.com/gopherjs/vecty/elem"
	"gitlab.com/MikeTTh/homeserver/dash/clientside/clock"
	"gitlab.com/MikeTTh/homeserver/dash/clientside/status"
	"gitlab.com/MikeTTh/homeserver/dash/clientside/weather"
	"sync"
	"syscall/js"
)

func main() {
	vecty.SetTitle("Dash")
	docelm := js.Global().Get("document").Get("documentElement")
	gofull := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		docelm.Call("requestFullscreen")
		return nil
	})
	docelm.Call("addEventListener", "click", gofull)
	vecty.RenderBody(&PageView{})
	var wg sync.WaitGroup
	wg.Add(1)
	wg.Wait()
}

type PageView struct {
	vecty.Core
}

func (*PageView) Render() vecty.ComponentOrHTML {
	return elem.Body(
		elem.Div(
			vecty.Markup(vecty.Class("center")),
			&clock.PageView{},
			&status.PageView{},
			&weather.View{},
		),
	)
}
