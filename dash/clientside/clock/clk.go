package clock

import (
	"github.com/gopherjs/vecty"
	"github.com/gopherjs/vecty/elem"
	"time"
)

type PageView struct {
	vecty.Core
}

func (p *PageView) Render() vecty.ComponentOrHTML {
	t := time.Now()
	go func() {
		time.Sleep(time.Second)
		vecty.Rerender(p)
	}()
	return elem.Div(
		elem.Paragraph(
			vecty.Markup(vecty.Style("font-size", "15pt")),
			vecty.Text(t.Format("2006-01-02, 15:04:05")),
		),
	)
}
