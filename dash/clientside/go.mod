module gitlab.com/MikeTTh/homeserver/dash/clientside

go 1.13

replace gitlab.com/MikeTTh/homeserver/dash/clientside => ./

replace gitlab.com/MikeTTh/homeserver => ../../

require (
	github.com/gopherjs/vecty v0.0.0-20200328200803-52636d1f7aba
	gitlab.com/MikeTTh/homeserver v0.0.0-20200812224650-60c9aa93d8eb
)
