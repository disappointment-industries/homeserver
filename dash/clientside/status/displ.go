package status

import (
	"encoding/json"
	"fmt"
	"github.com/gopherjs/vecty"
	"github.com/gopherjs/vecty/elem"
	"gitlab.com/MikeTTh/homeserver/dash"
	"gitlab.com/MikeTTh/homeserver/dash/clientside/elements"
	"io/ioutil"
	"net/http"
	"syscall/js"
	"time"
)

type PageView struct {
	vecty.Core
}

var stat = &dash.StatusData{}

func getData() {
	url := js.Global().Get("window").Get("location").Get("href").String()
	if resp, e := http.Get(url + "status"); e == nil {
		if bod, err := ioutil.ReadAll(resp.Body); err == nil {
			if er := json.Unmarshal(bod, stat); er != nil {
				fmt.Println("Failed to unmarshal data: ", er)
			}
		}
		_ = resp.Body.Close()
	}
}

type displayNode struct {
	Appended  bool
	Name      string
	CpuProg   *elements.ProgressBar
	RamProg   *elements.ProgressBar
	DataProg  *elements.ProgressBar
	Disks     []*elements.ProgressBar
	DiskProgs vecty.List
}

var nodeList []displayNode

type pWrapper struct {
	vecty.Core
	WrappedChild vecty.MarkupOrChild
}

func (p *pWrapper) Render() vecty.ComponentOrHTML {
	return elem.Paragraph(p.WrappedChild)
}

func update() {
	for i, n := range stat.Nodes {
		if i >= len(nodeList) {
			nodeList = append(nodeList, displayNode{
				Name:     "",
				CpuProg:  &elements.ProgressBar{Percent: 0, Text: "CPU: "},
				RamProg:  &elements.ProgressBar{Percent: 0, Text: "RAM: "},
				DataProg: &elements.ProgressBar{Percent: 0, Text: "TX:"},
				Disks:    make([]*elements.ProgressBar, 0),
			})
		}

		nodeList[i].Name = n.Name
		nodeList[i].CpuProg.Percent = n.CPU
		nodeList[i].RamProg.Percent = float64(n.MemUsed) / float64(n.MemTotal)
		nodeList[i].RamProg.Text = fmt.Sprintf("RAM: %.2f GB / %.2f GB, ", float64(n.MemUsed)/1024/1024/1024, float64(n.MemTotal)/1024/1024/1024)
		nodeList[i].DataProg.Percent = float64(n.Sent) / float64(32*1024*1024*1024)
		nodeList[i].DataProg.Text = fmt.Sprintf("TX: %.3f GB ", float64(n.Sent)/1024/1024/1024)
		for j, d := range n.Storage {
			if j >= len(nodeList[i].Disks) {
				nodeList[i].Disks = append(nodeList[i].Disks, &elements.ProgressBar{Text: fmt.Sprintf("%s: %.2f GB / %.2f GB, ", d.Name, float64(d.Used)/(1024*1024*1024), float64(d.Used+d.Avail)/(1024*1024*1024)), Percent: float64(d.Used) / float64(d.Used+d.Avail)})
				nodeList[i].DiskProgs = append(nodeList[i].DiskProgs, &pWrapper{WrappedChild: nodeList[i].Disks[j]})
			} else {
				nodeList[i].Disks[j].Percent = float64(d.Used) / float64(d.Used+d.Avail)
				nodeList[i].Disks[j].Text = fmt.Sprintf("%s: %.2f GB / %.2f GB, ", d.Name, float64(d.Used)/(1024*1024*1024), float64(d.Used+d.Avail)/(1024*1024*1024))
			}
		}

	}
}

func (p *PageView) Render() vecty.ComponentOrHTML {
	if len(stat.Nodes) == 0 {
		getData()
		update()
	}

	go func() {
		time.Sleep(5 * time.Second)
		getData()
		update()
		vecty.Rerender(p)
	}()

	var nodes vecty.List

	for i := range nodeList {
		var data vecty.List
		if nodeList[i].DataProg.Percent != 0 {
			data = append(data, elem.Paragraph(
				nodeList[i].DataProg,
			))
		}

		nodes = append(nodes, elem.Div(
			vecty.Markup(vecty.Style("width", "100%")),
			elem.Div(
				vecty.Markup(vecty.Markup(vecty.Style("margin", "1em"))),
				elem.Heading3(vecty.Text(nodeList[i].Name)),
				elem.Paragraph(
					nodeList[i].CpuProg,
				),
				elem.Paragraph(
					nodeList[i].RamProg,
				),
				data,
				nodeList[i].DiskProgs,
			),
		))
	}

	return elem.Div(
		nodes,
	)
}
