package elements

import (
	"fmt"
	"github.com/gopherjs/vecty"
	"github.com/gopherjs/vecty/elem"
)

type ProgressBar struct {
	vecty.Core
	Percent float64
	Text    string
}

// TODO: Implement rerendering stuff

func (p *ProgressBar) Render() vecty.ComponentOrHTML {
	var txt vecty.ComponentOrHTML
	if p.Text != "" {
		txt = elem.Div(
			vecty.Markup(
				vecty.Style("background-color", "#0000"),
				vecty.Style("text-align", "left"),
				vecty.Style("text-shadow", "0.1em 0em 0.2em #000, -0.1em 0em 0.2em #000"),
				vecty.Style("white-space", "nowrap"),
				vecty.Style("margin", "1em"),
			),
			vecty.Text(fmt.Sprintf("%s%.2f%%", p.Text, p.Percent*100)),
		)
	} else {
		txt = elem.Div(vecty.Markup(vecty.Style("position", "absolute")))
	}
	return elem.Div(
		vecty.Markup(
			vecty.Style("width", "100%"),
			vecty.Style("border-radius", "0.5em"),
			vecty.Style("background-color", "#000"),
		),
		elem.Div(
			vecty.Markup(
				vecty.Style("border-radius", "0.5em"),
				vecty.Style("transition", "width 1s"),
				vecty.Style("background-color", "#50fa7b"),
				vecty.Style("width", fmt.Sprintf("%f%%", p.Percent*100.0)),
			),
			txt,
		),
	)
}
