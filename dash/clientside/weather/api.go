package weather

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

var apiKey = "9e97ef2124f133c9cf98fea2fb9c8968"

type fiveday struct {
	Cod     string
	Message int
	Cnt     int
	List    []struct {
		UnixDate int `json:"dt"`
		Main     struct {
			Temp      float64
			FeelsLike float64 `json:"feels_like"`
			TempMin   float64 `json:"temp_min"`
			TempMax   float64 `json:"temp_max"`
			Pressure  float64
		}
		Weather []struct {
			Id          int
			Main        string
			Description string
			Icon        string
		}
		Clouds struct {
			All float64
		}
		Wind struct {
			Speed float64
			Deg   float64
		}
	}
	City struct {
		Id   int
		Name string
	}
}

func getWeather() fiveday {
	if resp, err := http.Get("https://api.openweathermap.org/data/2.5/forecast?q=Budapest,hu&mode=json&units=metric&appid=" + apiKey); err == nil {
		if bod, err := ioutil.ReadAll(resp.Body); err == nil {
			var fd fiveday
			if err = json.Unmarshal(bod, &fd); err != nil {
				fmt.Println(err)
			} else {
				return fd
			}
		} else {
			fmt.Println(err)
		}
	} else {
		fmt.Println(err)
	}
	return fiveday{Cod: "err"}
}
