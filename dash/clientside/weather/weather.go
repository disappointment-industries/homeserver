package weather

import (
	"fmt"
	"github.com/gopherjs/vecty"
	"github.com/gopherjs/vecty/elem"
	"time"
)

type View struct {
	vecty.Core
}

var fd = fiveday{Cod: "err"}

func (w *View) Render() vecty.ComponentOrHTML {
	go func() {
		time.Sleep(time.Minute)
		fd = getWeather()
		vecty.Rerender(w)
	}()

	str := ""

	if fd.Cod != "err" {
		str = fmt.Sprintf("it is currently %g °C and %s in %s", fd.List[0].Main.Temp, fd.List[0].Weather[0].Description, fd.City.Name)
	} else {
		go func() {
			fd = getWeather()
			vecty.Rerender(w)
		}()
	}

	return elem.Div(
		elem.Paragraph(
			vecty.Text(str),
		),
	)
}
