package dash

import (
	"time"
)

type Disk struct {
	Name  string
	Avail uint64
	Used  uint64
}

type Node struct {
	Name     string
	CPU      float64
	MemUsed  uint64
	MemTotal uint64
	Sent     uint64
	Storage  []Disk
}

type StatusData struct {
	Nodes []Node
	Time  time.Time
}
