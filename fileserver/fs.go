package fileserver

import (
	"github.com/google/logger"
	"gitlab.com/MikeTTh/homeserver/fourohfour"
	"io"
	"net/http"
	"os"
)

const (
	preBod = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/res/defTheme.css" />
    <link href="https://fonts.googleapis.com/css2?family=Fira+Code&family=Roboto&display=swap" rel="stylesheet">
    <link rel="icon"
          type="image/png"
          href="/res/favicon.png" />
    <meta name="theme-color" content="#282629" />
    <title>Mike's website</title>
    <style>
		body {
			margin: 1em;
		}
        * {
            font-family: 'Fira Code', monospace;
        }
    </style>
</head>
<body>`
	postBod = `
</body>
</html>`
)

type FileServer func(root http.FileSystem) http.Handler

func Fs(d http.FileSystem) http.Handler {
	fs := http.FileServer(d)

	var handler http.HandlerFunc
	handler = func(w http.ResponseWriter, r *http.Request) {
		var (
			url   = r.URL.Path
			isDir = url[len(url)-1] == '/'
		)

		if _, err := d.Open(url + "index.html"); err == nil {
			isDir = false
		}

		if _, err := d.Open(url); os.IsNotExist(err) {
			fourohfour.Handle404(w, r)
			return
		}

		if isDir {
			_, err := io.WriteString(w, preBod)
			if err != nil {
				logger.Errorf("FileServer: %v", err)
			}
		}

		fs.ServeHTTP(w, r)

		if isDir {
			_, err := io.WriteString(w, postBod)
			if err != nil {
				logger.Errorf("FileServer: %v", err)
			}
		}
	}
	return handler
}
